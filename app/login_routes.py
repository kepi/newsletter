from flask import Blueprint, request, redirect, render_template, url_for, flash, current_app, jsonify
from flask_login import login_required, login_user, logout_user, current_user
from app.exceptions import InvalidRegistrationData, CaptchaFailed
import requests
from app import db
from app.models import User, InGroup, Group, get_all_group_names, get_groups_by_user, user_loader, get_users_from_groups
import secrets
from werkzeug.security import check_password_hash
import datetime
from app.decorators import log_activity, teacher_login_required
from app.email_routes import send_mail

login_routes = Blueprint('login_routes', __name__)

def check_captcha(captcha_response):
    return
    data = {"response": captcha_response, "secret": current_app.config["HCAPTCHA_SECRET"]}
    if requests.post("https://hcaptcha.com/siteverify", data=data).json()["success"]:
        return
    else:
        raise CaptchaFailed

def constructMessage(text):
    return jsonify(results=[{"id":"disabledOption", "text":text, "disabled":True}],
            pagination={"more":False})

def search_users(query, users):
    """
    Replaces the default select2 search function that runs client side since
    user data is loaded via ajax and paginated
    """
    query = query.lower().strip()

    # TODO make this nicer
    matching_users = []
    for user in users:
        if f"{user.first_name} {user.last_name}".lower().strip().find(query) != -1:
            matching_users.append(user)

    return matching_users

@login_routes.route("/get_users_ajax", methods=["GET"])
@teacher_login_required
def get_users_ajax():
    if "group" in request.args:
        if request.args["in_group"] == "True":
            # Get all users that are in the selected group
            users = get_users_from_groups([request.args["group"]], students=True, parents=True)
        else:
            # Get all users that are not in the selected group
            in_group = get_users_from_groups([request.args["group"]], students=True, parents=True)
            users = db.session.query(User).filter(User.email.in_([user.email for user in in_group]) == False).all()
        users = search_users(request.args["query"], users)

        # Split that list into different pages
        pagesize = current_app.config["SELECT2_PAGE_SIZE"]
        pages = [users[pagesize*i:min(len(users), pagesize*(i+1))] for i in range(1+int(len(users)/pagesize))]
        users = pages[min(int(request.args["page"]), len(pages)-1)]

        results = [{
            "id":user.email,  # this is pretty much the only point where emails are visible to the client
            "text":f"{user.first_name} {user.last_name}",
            "html":f"{user.first_name} {user.last_name} <em>{user.role}</em>",
            "title":f"{user.first_name} {user.last_name}"
            } for user in users]
        if len(results) == 0:
            if request.args["in_group"] == "True":
                return constructMessage("Diese Gruppe hat keine Mitglieder")
            else:
                return constructMessage("Alle Nutzer sind Mitglieder dieser Gruppe")
        return jsonify(results=results, pagination={"more":len(pages) != 1})
    else:
        return constructMessage("Wählen sie zuerst eine Gruppe aus")

@login_routes.route("/manage_groups", methods=["GET", "POST"])
@teacher_login_required
def manage_groups():
    if request.method == "GET":
        return render_template("manage_groups.html", groups=get_all_group_names())
    else:
        flashed_messages = []
        if request.form["mode"] == "create":
            name = request.form["createName"]

            if name in get_all_group_names():
                flashed_messages.append("Der angegebene Gruppenname existiert bereits")
            else:
                db.session.add(Group(name))
                db.session.commit()

        elif request.form["mode"] == "rename":
            oldname = request.form["renameGroup"]
            newname = request.form["renameName"]
            
            if newname in get_all_group_names():
                flashed_messages.append("Der angegebene Gruppenname existiert bereits")
            else:
                # modify Group entries
                group = db.session.query(Group).filter(Group.name == oldname).first()
                group.name = newname
                # InGroup
                ingroup_rows = db.session.query(InGroup).filter(InGroup.group == oldname).all()
                for ingroup_row in ingroup_rows:
                    ingroup_row.group = newname

                db.session.commit()

        elif request.form["mode"] == "delete":
            names = request.form.getlist("deleteGroups")

            # delete Group/InGroup Entries
            db.session.query(Group).filter(Group.name.in_(names)).delete(synchronize_session="fetch")
            db.session.query(InGroup).filter(InGroup.group.in_(names)).delete(synchronize_session="fetch")
            db.session.commit()

        elif request.form["mode"] == "edit":
            groupname = request.form["editGroup"]
            group_id = db.session.query(Group.id).filter(Group.name == groupname).first().id
            add_users = request.form.getlist("addToGroup")
            remove_users = request.form.getlist("removeFromGroup")

            for user in add_users:
                db.session.add(InGroup(email=user, group=group_id))

            for user in remove_users:
                db.session.query(InGroup).filter(InGroup.email == user, InGroup.group == group_id).delete()
            db.session.commit()

        return jsonify(result=get_all_group_names(), messages=flashed_messages)

@login_routes.route("/settings", methods=["POST", "GET"])
@login_required
def settings():
    if request.method == "POST":
        if current_user.unsubscribe_token == request.form["token"]:
            if "delete" in request.form:
                db.session.query(User).filter(User.email == current_user.email).delete()
                db.session.query(InGroup).filter(
                    InGroup.email == current_user.email
                ).delete()
                db.session.commit()
                flash(
                    "Sie werden keine Emails mehr erhalten. Alle Ihre Daten wurden von unserer Datenbank gelöscht."
                )
                return redirect(url_for('login_routes.login'))
            else:
                # current_user.update()
                db.session.query(InGroup).filter(
                    InGroup.email == current_user.email
                ).delete()
                # Only use groups that exist in db.
                all_groups = get_all_group_names()
                groups = [
                    group
                    for group in request.form["groups"].split(",")
                    if group in all_groups
                ]
                for group in groups:
                    db.session.add(InGroup(current_user.email, db.session.query(Group.id).filter(Group.name == group).first()[0]))
                db.session.commit()

                flash("Ihre Daten wurden geändert")
                return render_template(
                    "settings.html",
                    token=current_user.unsubscribe_token,
                    groups=get_groups_by_user(current_user),
                )

        else:
            flash("Invalid link")
            return redirect(url_for('login_routes.login'))
    else:
        if 'token' in request.args:
            return render_template(
                "settings.html",
                token=request.args["token"],
                groups=get_groups_by_user(current_user),
            )
        elif current_user.is_authenticated():
            # If an authenticated user opens /settings
            password_reset_token = secrets.token_hex(32)
            user.set_password_reset_token(password_reset_token)
            return render_template(
                "settings.html",
                token=password_reset_token,
                groups=get_groups_by_user(current_user),
            )
        else:
            return render_template("settings.html")


@login_routes.route("/confirm")
@login_required
def confirm():
    token=request.args["token"]
    if (
        current_user.confirmation_token == token
        and datetime.datetime.now() - current_user.registration_time
        < current_app.config["LINK_EXPIRATION_TIME"]
    ):
        current_user.confirm()
        flash("Sie haben ihre Emailaddresse bestätigt. Sie sind erfolgreich beim Newsletter angemeldet.")
        return redirect(url_for('public_routes.home'))
    else:
        flash("Der Bestätigungslink ist ungültig. Stellen Sie sicher, dass Sie entweder auf den Link in der Email geklickt haben, oder dass Sie den gesammten Link kopiert haben")
        return redirect(url_for('public_routes.home'))


@login_routes.route("/request_new_password", methods=["POST", "GET"])
def request_new_password():
    if request.method == "POST":
        try:
            check_captcha(request.form["h-captcha-response"])
            email = request.form["email"]
            user = user_loader(email)
            if user is not None:
                password_reset_token = secrets.token_hex(32)
                user.set_password_reset_token(password_reset_token)
                # TODO style mail
                send_mail(
                    users=[user],
                    subject="Passwort zurücksetzen",
                    message="<a href=" + url_for(
                        "login_routes.set_new_password",
                        email=email,
                        token=password_reset_token,
                        _external=True,
                    ) + ">Email bestätigen</a>"
                )
            flash(
                f"Ein Link zum Zurücksetzen des Passworts wurde an {email} versendet"
            )
        except CaptchaFailed:
            flash("Bitte füllen Sie das Captcha aus")

    return render_template("request_new_password.html")


@login_routes.route("/set_new_password", methods=["POST", "GET"])
def set_new_password():
    if request.method == "POST":
        user = user_loader(request.form["email"])
        if (
            user is not None
            and check_password_hash(user.password_reset_token, request.form["token"])
            and datetime.datetime.now() - user.password_reset_time
            < current_app.config["LINK_EXPIRATION_TIME"]
        ):
            user.set_new_password(password=request.form["password"])
            flash("Ihr Passwort wurde geändert. Bitte Melden Sie sich an")
            return redirect(url_for('login_routes.login'))
        else:
            flash("Link abgelaufen oder falsch")
            return render_template(
                "set_new_password.html",
                email=request.args["email"],
                token=request.args["token"],
            )

    else:
        return render_template(
            "set_new_password.html",
            email=request.args["email"],
            token=request.args["token"],
        )


@login_routes.route("/register", methods=["POST", "GET"])
def register():
    if request.method == "POST":
        user = user_loader(request.form["email"])
        if user is not None:
            flash("Sie sind schon registriert, bitte melden Sie sich an")
            return redirect(url_for('login_routes.login'))
        else:
            try:
                check_captcha(request.form["h-captcha-response"])
                try:
                    if current_app.config["TEACHER_MAIL"] in request.form["email"]:
                        user = User(
                            email=request.form["email"],
                            first_name=request.form["first_name"],
                            last_name=request.form["last_name"],
                            role="teacher",
                            password=request.form["password"],
                        )
                    else:
                        user = User(
                            email=request.form["email"],
                            first_name=request.form["first_name"],
                            last_name=request.form["last_name"],
                            role=request.form["role"],
                        )
                except KeyError:
                    raise InvalidRegistrationData

                login_user(user)

                confirmation_token = secrets.token_hex(16)
                unsubscribe_token = secrets.token_hex(16)
                current_user.set_tokens(confirmation_token, unsubscribe_token)
                send_mail(
                    [current_user],
                    subject="Bestätigen Sie ihre Emailaddresse",
                    message="<a href={link}>Bestätigen</a>".format(
                        link=url_for(
                            "login_routes.confirm", 
                            token=confirmation_token, 
                            _external=True)
                    )
                )

                # Add user to selected groups
                # Only use groups that exist in db.
                all_groups = get_all_group_names()
                groups = [
                    group
                    for group in request.form["groups"].split(",")
                    if group in all_groups
                ]
                for group in groups:
                    db.session.add(InGroup(request.form["email"], db.session.query(Group.id).filter(Group.name == group).first()[0]))
                db.session.add(current_user)
                db.session.commit()
                flash(
                    "Bitte bestätigen Sie ihre Emailaddresse. Wenn Sie dies 24h nicht tun, werden Sie automatisch vom Newletter abgemeldet."
                )
                return redirect(url_for('login_routes.login'))

            except InvalidRegistrationData:
                flash("Error")
            except CaptchaFailed:
                flash("Bitte füllen Sie das Captcha aus")

            return render_template(
                "register.html",
                teacher_login=current_app.config["TEACHER_MAIL"] in request.form["email"],
                groups=get_all_group_names(),
                **{key: request.form[key] for key in request.form if "groups" != key},
            )
    else:
        return redirect(url_for('login_routes.login'))


@login_routes.route("/login", methods=["POST", "GET"])
@log_activity
def login():
    if request.method == "POST":
        user = user_loader(request.form["email"])
        if user is None:
            flash("Diese Emailaddresse ist noch nicht registriert")
            return render_template(
                "register.html",
                groups=get_all_group_names(),
                email=request.form["email"],
                teacher_login=current_app.config["TEACHER_MAIL"] in request.form["email"]
            )

        elif user.role == 'teacher':
            if "password" in request.form:
                password = request.form["password"]
                if check_password_hash(user.password, password):
                    login_user(user)
                    return redirect(request.args.get("next") or url_for("public_routes.home"))
                else:
                    flash("E-Mail oder Passwort falsch")
            return render_template(
                "login.html",
                email=request.form["email"],
                teacher_login=True,
                password_reset_link=url_for("login_routes.request_new_password"),
            )

        else:
            login_user(user)
            return redirect(request.args.get("next") or url_for("public_routes.home"))
    else:
        return render_template("login.html")


@login_routes.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Sie wurden abgemeldet")
    return redirect(request.args.get("next") or url_for('login_routes.login'))