class InvalidRegistrationData(Exception):
    pass


class CaptchaFailed(Exception):
    pass