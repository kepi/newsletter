// Declare global variables
let attachments = [];
let attachments_visible = false;
let max_attachment_size = 1024 * 1024 * 10; // 10MB
let socket = io();
let num_mails = 0;
let num_mails_sent = 0;
let num_mails_failed = 0;

$(document).ready(function() {
	$("#anhang").on("change", attachAttachment);
	$("#toggleAttachments").tooltip({"trigger":"manual"})

	// Hide the Attachment list(default state)
	toggleAttachmentVisibility(show=false);

	// Initialize Websockets
    socket.connect();
    socket.on("init_status_list", init_status_table);
    socket.on("update_status_list", update_status_table);
});

function attachAttachment(event){
	let files = event.target.files

	// Push the newly added Files to the Attachments Array
	for(i = 0; i < files.length; i++){
		attachments.push(files[i]);
	}
	// Check if the total size of all attachments exceeds the maximum transfer size
	if (totalAttachmentSize() > max_attachment_size){
		alert("Ihre Anhänge sind zu gross(" + numBytesToMB(totalAttachmentSize()) + "MB > " + numBytesToMB(max_attachment_size) + "MB)");
	}
	renderAttachments();
	toggleAttachmentVisibility(show=true);
}

function numBytesToMB(num_bytes){
	num_mb = Math.round(num_bytes / 1024 / 1024 * 10) / 10;
	return num_mb
}

function detachAttachment(index){
	// remove the index from the array of Attachments
	attachments.splice(index, 1);

	//Rerender the Attachments because Button indices may have changed
	renderAttachments();
}

function renderAttachments(){
	anhaenge_liste = $("#anhaenge_liste");
	anhaenge_liste.empty()
	for(i = 0; i < attachments.length; i++){
		// Every list element consists of the filename and a delete button
		let li = "<div class='list-group-item'>";
		li += "<span style='line-height: 36px;'>";
		li += attachments[i].name + " ";
		li += numBytesToMB(attachments[i].size) + "MB ";
		li += "</span>";
		li += "<button onclick='detachAttachment(" + i + ");' class='btn btn-danger rmButton'><span class='glyphicon glyphicon-trash'></span></button>";
		li += "</div>";
		anhaenge_liste.append(li);
	}

	if (attachments.length == 0){
		toggleAttachmentVisibility(show=false);
	}
}


function toggleAttachmentVisibility(show=undefined){
	/*
	Hides/shows the Div containing the attachments. Unless force is true,
	the state will not be changed if there are no attachments as showing
	an empty list looks stupid.
	*/

	if (show !== undefined) {
		attachments_visible = show;
	}
	else {
		attachments_visible = !attachments_visible;
	}

	// Change the Button text to represent current state
	if(attachments_visible){
		$(".collapse").collapse("show")
		$("#toggleAttachments").html("<span class='glyphicon glyphicon-triangle-bottom'></span>");
	}
	else{
		$(".collapse").collapse("hide")
		$("#toggleAttachments").html("<span class='glyphicon glyphicon-triangle-left'></span>");
	}
}

function totalAttachmentSize(){
	let total_size = 0;
	for (i = 0; i < attachments.length; i++){
		total_size += attachments[i].size;
	}
	return total_size
}

function init_status_table(){
	var content = `
		<div class="progress">
			<div class="progress-bar progress-bar-success" id="progressbarSuccess" role="progressbar" aria-valuenow="0"
				aria-valuemin="0" aria-valuemax="` + num_mails + `" style="width:0%">
		    0%
  			</div>
  			<div class="progress-bar progress-bar-danger" id="progressbarFailure" role="progressbar" aria-valuenow="0"
				aria-valuemin="0" aria-valuemax="` + num_mails + `" style="width:0%">
		    0%
  			</div>
		</div> 
		<input class='pretty_textbox' id='searchBar' type='text' placeholder='Filtern sie die Ergebnisse...'>
		<table class='table table-striped table-hover table-responsive' id='statusTable'>
			<thead>
				<tr>
					<th>Status</th>
					<th>Empfänger</th>
					<th>Fehler</th>
				</tr>
			</thead>
			<tbody id="statusTableBody">
			</tbody>
		</table>`;


	// Table Content is added later through dynamic updates
	$("#statusList").html(content);

	// JQuery Code for the Search bar
	$("#searchBar").on("keyup", function() {
	    var value = $(this).val().toLowerCase();
	    $("#statusTable .statusRow").filter(function() {
	      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
	    });
  	});      
}

function update_status_table(status){
	if (status[0]){
		table_row = `
			<tr class='success statusRow'  data-toggle='tooltip' title='Okay' data-placement='right'>
				<td><img src='static/img/success.png'></td>
				<td>` + status[1] + `</td>
				<td>Kein Fehler</td>
			</tr>`;
		num_mails_sent += 1;
	}
	else{
		table_row = `
			<tr class='failure statusRow'  data-toggle='tooltip' title='Okay' data-placement='right'>
				<td><img src='static/img/failure.png'></td>
				<td>` + status[1] + `</td>
				<td>` + status[2] + `</td>
			</tr>`;
		num_mails_failed += 1;
	}
	$("#statusTableBody").append(table_row);
	$("#statusText").text(num_mails_sent  + "/" + num_mails + " Emails erfolgreich versandt, " + num_mails_failed + " fehlgeschlagen");
	let percent_failed = Math.round(num_mails_failed / num_mails * 100);
	let percent_success = Math.round(num_mails_sent / num_mails * 100);

	$("#progressbarSuccess").text(percent_success+"%").attr("aria-valuenow", num_mails_sent).css({"width":percent_success+"%"});
	$("#progressbarFailure").text(percent_failed + "%").attr("aria-valuenow", num_mails_failed).css({"width":percent_failed+"%"});
}


function send(){
	//read Inputs
	let groups = $("#groupSelector").val();
	let subject = $("#betreff").val();
	let alt_content = $("#normal-output").text();
	let content = $("#html-output").html();
	let parentsOrStudents = $("#ParentStudentSelector input:radio:checked").val();
	let parents = parentsOrStudents == "all" || parentsOrStudents == "parents";
	let students = parentsOrStudents == "all" || parentsOrStudents == "students";

	// Structure the data to send
	let fd = new FormData();
	fd.append("subject", subject);
	fd.append("alt_content", alt_content);
	fd.append("content", content);
	fd.append("parents", parents);
	fd.append("students", students);
	fd.append("sid", socket.id);

	// Append all attachments to the Form
	for(i = 0; i < attachments.length; i++){
		fd.append("attachments", attachments[i])
	}
	// Add Groups to send the message to
	for (i = 0; i < groups.length; i++){
		fd.append("groups", groups[i]);
	}

	// Post to /emailsenden
	$.ajax({
		url: "/emailsenden",
		method: "POST",
		async: false,
		data: fd,
		processData: false,
        contentType: false,
        cache: false
	});
}

function getModalContent(){
	if (totalAttachmentSize() > max_attachment_size){
		alert("Ihre Anhänge sind zu gross. (" + numBytesToMB(totalAttachmentSize()) + "MB > " + numBytesToMB(max_attachment_size) + "MB)");
	}
	else{
		HoldOn.open({
			theme:"sk-cube-grid"
		});

		let groups = $("#groupSelector").val();
		let parentsOrStudents = $("#ParentStudentSelector input:radio:checked").val();
		let parents = parentsOrStudents == "all" || parentsOrStudents == "parents";
		let students = parentsOrStudents == "all" || parentsOrStudents == "students";


		let fd = new FormData();
		fd.append("parents", parents);
		fd.append("students", students);

		for(i = 0; i < groups.length; i++){
			fd.append("groups", groups[i]);
		}

		// Get all the usernames to have the User confirm that they indeed want to send
		// to these people
		$.ajax({
			url: "/getUserFromGroup",
			method: "POST",
			async: false,
			data: fd,
			processData: false,
	        contentType: false,
	        cache: false,
			complete: function(data, status){
				if(status == "success"){
					let usernames = JSON.parse(data.responseText).result;

					// Fill the data into the Popup
					$("#numSelected").text(usernames.length);
					let list = $("#selectedUsers");
					list.empty();
					for(i = 0; i < usernames.length; i++){
						list.append("<li class='list-group-item' id='selectedUser'>" + usernames[i] + "</li>");
					}
					num_mails = usernames.length;
					num_mails_sent = 0;
					num_mails_failed = 0;

					//Display the Popup
					$("#confirmPopup").modal("show");
					HoldOn.close();
				}
				else{
					alert("Fehler beim Kontaktieren des Servers");
					console.log(data);
					HoldOn.close();
					return
				}
			}
		});
	}
}

// Initialize pell on an HTMLElement
pell.init({
	// <HTMLElement>, required
	element: document.getElementById('editor'),

	// <Function>, required
	// Use the output html, triggered by element's `oninput` event
	onChange: html => console.log(html),

	// <string>, optional, default = 'div'
	// Instructs the editor which element to inject via the return key
	defaultParagraphSeparator: 'div',

	// <boolean>, optional, default = false
	// Outputs <span style="font-weight: bold;"></span> instead of <b></b>
	styleWithCSS: false,

	// <Array[string | Object]>, string if overwriting, object if customizing/creating
	// action.name<string> (only required if overwriting)
	// action.icon<string> (optional if overwriting, required if custom action)
	// action.title<string> (optional)
	// action.result<Function> (required)
	// Specify the actions you specifically want (in order)
	onChange: html => {
		let html_out = $("#html-output")
		let normal_out = $("#normal-output")
		let div = document.createElement("div")

		// Der Parser im Browser wird benutzt um HTML tags zu entfernen
		html_out.html(html)
		div.innerHTML = html
		normal_out.textContent = div.textContent;

	},
	defaultParagraphSeparator: 'p',
	styleWithCSS: true,
	actions: [
		'bold',
		'italic',
		'underline',
		'strikethrough',
		'heading1',
		'heading2',
		'paragraph',
		'quote',
		'olist',
		'ulist',
		'code',
		'line',
		'link',
		{
			name: 'image',
			result: () => {
				const url = window.prompt('Enter the image URL')
				if (url) this.pell.exec('insertImage', url)
			}
		}
	],

	// classes<Array[string]> (optional)
	// Choose your custom class names
	classes: {
		actionbar: 'pell-actionbar',
		button: 'pell-button',
		content: 'pell-content',
		selected: 'pell-button-selected'
	}
})

// Execute a document command, see reference:
// https://developer.mozilla.org/en/docs/Web/API/Document/execCommand
// this is just `document.execCommand(command, false, value)`
//pell.exec(command < string >, value<string>)