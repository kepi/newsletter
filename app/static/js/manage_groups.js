$(document).ready(function(){
	$('#chooseGroupSelector').on('select2:selecting', function(e) {
    	$("#addSelector").val(null).change();
    	$("#removeSelector").val(null).change();
  	});	
});

function updateSelect2Options(options){
	// Empty all Select2 Boxes in the Page
	$(".groupSelector").each(function(){
        $(this).empty();
    });

	// Add all the updated options back in
	for(i = 0; i < options.length; i++){
		$(".groupSelector").each(function(){
			var newOption = new Option(options[i], options[i], false, false);
        	$(this).append(newOption).change();
    	});
	}
}

function flash_messages(messages){
	// Flash messages without reloading the page
	$("#messages").empty();
	for(i = 0; i < messages.length; i++){
		message = `
			<div class="pretty_message">
				` + messages[i] + `
			</div>`;
		$("#messages").append(message);
	}
}

function send(mode){
	HoldOn.open({
		theme:"sk-cube-grid"
	});

	let fd = new FormData();
	fd.append("mode", mode);
	fd.append("createName", $("#createName").val());
	fd.append("renameName", $("#renameName").val());
	fd.append("renameGroup", $("#renameGroupSelector").val());
	fd.append("editGroup", $("#chooseGroupSelector").val());

	let deleteGroups = $("#deleteGroupSelector").val();
	let removeFromGroup = $("#removeSelector").val();
	let addToGroup = $("#addSelector").val();
	
	for(i = 0; i < deleteGroups.length; i++){
		fd.append("deleteGroups", deleteGroups[i]);
	}
	for(i = 0; i < addToGroup.length; i++){
		fd.append("addToGroup", addToGroup[i]);
	}
	for(i = 0; i < removeFromGroup.length; i++){
		fd.append("removeFromGroup", removeFromGroup[i]);
	}

	$.ajax({
		url: "/manage_groups",
		method: "POST",
		data: fd,
		processData: false,
        contentType: false,
        cache: false,
		complete: function(data, status){
			if(status=="success"){
				let response = JSON.parse(data.responseText);
				updateSelect2Options(response.result);
				flash_messages(response.messages);
			}
			else{
				alert("Fehler beim Kontaktieren des Servers");
				console.log(status)
				console.log(data);
			}
		}
	});
	// Clear all Selections because the groups might have been deleted
	$(".groupSelector").each(function(){
        $(this).val(null).change();
    });
    $("#renameName").val(null);

	HoldOn.close();
	return false;
}