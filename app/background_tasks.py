import datetime
from flask import current_app
from app import db
from app.models import User, InGroup


def delete_unconfirmed_users(app):
	max_time = datetime.datetime.now() - app.config["LINK_EXPIRATION_TIME"]
	with app.app_context():
		unconfirmed_users = db.session.query(User).filter(User.is_confirmed == False, User.registration_time < max_time).all()
		db.session.query(User).filter(User.is_confirmed == False, User.registration_time < max_time).delete(synchronize_session="fetch")
		db.session.query(InGroup).filter(InGroup.email.in_([user.email for user in unconfirmed_users])).delete(synchronize_session="fetch")
		db.session.commit()	

