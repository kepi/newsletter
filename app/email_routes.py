from flask import Blueprint, render_template, url_for, request, jsonify, current_app
from app.models import get_all_group_names, get_users_from_groups
from werkzeug.utils import secure_filename
import mimetypes
from flask_login import current_user
from app.decorators import log_activity, teacher_login_required
from flask_mail import Message
from app.exceptions import InvalidRegistrationData
from app import mail, socketio

email_routes = Blueprint('email_routes', __name__)

@email_routes.route("/write")
@log_activity
@teacher_login_required
def schreiben():
    return render_template(
        "write.html",
        post_to=url_for("email_routes.senden"),
        sender=current_app.config["MAIL_USERNAME"],
        groups=get_all_group_names()
    )


def read_attachments(files):
    """
    Reads all the Files in file_list and extracts information such as
    Mimetype etc. This is done to avoid having to open/read the file
    for every message
    """
    if files is None:
        files = []
    filenames = [secure_filename(file_.filename) for file_ in files]
    file_data = [file_.read() for file_ in files]
    mimetypes_ = [
        mimetypes.MimeTypes().guess_type(filename)[0] or "application/octet-stream"
        for filename in filenames
    ]
    return zip(file_data, filenames, mimetypes_)


def send_mail(users, subject, message, teacher_sid=None, reply_to=None, attachments=None, raise_on_exception=False):
    """Send email to users. Handles unsubscribe link

    Args:
        users (list): List of user objects to send the message to.
        subject (str): Subject of message
        message (str): Message or body of email. Can be html.
        teacher_sid (str): The Clients SocketID
        reply_to (str, optional): The emailadress the users should reply to. Defaults to None.
        attachments (list, optional): List of files to attach. Defaults to [].

    """
    print(teacher_sid)
    print("-"*20)

    attachments = read_attachments(attachments)
    if teacher_sid is not None:
        print("initiating status list")
        socketio.emit("init_status_list", room=teacher_sid)

    with mail.connect() as conn:
        for user in users:
            # Message headers
            msg = Message(
                recipients=[user.email],
                sender=('Kepi Newsletter' if current_user.is_anonymous else f"{current_user.first_name} {current_user.last_name}", "test@kepi.hercke.de"),
                subject=subject,
                reply_to=reply_to,
            )
            # Render message body
            msg.html = render_template(
                "email.html",
                message=message,
                unsubscribe_token=user.unsubscribe_token
            )

            # Add Attachments
            for (data, filename, mimetype) in attachments:
                msg.attach(data=data, content_type=mimetype, filename=filename)

            # Send message and set status
            try:
                conn.send(msg)
                success = True
                error_type = None
            except Exception as e:
                if raise_on_exception:
                    raise InvalidRegistrationData
                success = False
                error_type = type(e).__name__
            finally:
                status = [success, f"{user.first_name} {user.last_name}", error_type]
                if teacher_sid is not None:
                    print(f"sending to {teacher_sid}")
                    socketio.emit("update_status_list", status, room=teacher_sid)


@email_routes.route("/getUserFromGroup", methods=["POST"])
@teacher_login_required
def getUsers():
    """
    Before the Client sends the email to /emailsenden, the site prompts the Client to
    confirm that they indeed want to send the Mail to all the people in the selected
    groups, this site just sends the info for that
    """
    users = get_users_from_groups(
        request.form.getlist("groups"), 
        parents=request.form["parents"], 
        students=request.form["students"]
    )
    return jsonify(result=[f"{user.first_name} {user.last_name}" for user in users])


@email_routes.route("/emailsenden", methods=["POST"])
@teacher_login_required
def senden():
    users = get_users_from_groups(
        request.form.getlist("groups"),
        parents=request.form["parents"],
        students=request.form["students"],
    )
    send_mail(
        users=users,
        subject=request.form["subject"],
        message=request.form["content"],
        reply_to=current_user.email,
        teacher_sid=request.form["sid"],
        attachments=request.files.getlist("attachments"),
    )
    return 'Success'
