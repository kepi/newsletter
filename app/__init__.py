from flask import Flask
from flask_mail import Mail
from flask_socketio import SocketIO
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
from flask_static_digest import FlaskStaticDigest
import flask_assets
from config import ProductionConfig, DevelopmentConfig
import os

mail = Mail()
db = SQLAlchemy()
socketio = SocketIO()
scheduler = APScheduler()
login_manager = LoginManager()
assets = flask_assets.Environment()
flask_static_digest = FlaskStaticDigest()
login_manager.login_view = "login_routes.login"
login_manager.login_message = "Bitte melden sie sich an um diese Seite zu öffnen"

def create_app():
    app = Flask(__name__)
    # export FLASK_ENV=development
    if app.config["ENV"] == "production":
        app.config.from_object(ProductionConfig)
    else:
        app.config.from_object(DevelopmentConfig)

    db.init_app(app)
    mail.init_app(app)
    assets.init_app(app)
    socketio.init_app(app)
    scheduler.init_app(app)
    login_manager.init_app(app)

    with app.app_context():
        # Iterate recursively through all static files and minify them if necessary
        for directory in ["js", "styles"]:
            for file_name in os.listdir(os.path.join("app/static", directory)):
                path = os.path.join(directory, file_name)
                if file_name.endswith(".js"):
                    asset = flask_assets.Bundle(path, output=f"/build/js/{file_name}", filters="jsmin")
                elif file_name.endswith(".css"):
                    asset = flask_assets.Bundle(path, output=f"/build/css/{file_name}", filters="cssmin")
                else:
                    continue
                assets.register(file_name, asset)
                asset.build(force=True)


    flask_static_digest.init_app(app)


    from app.login_routes import login_routes as login_blueprint
    app.register_blueprint(login_blueprint)

    from app.email_routes import email_routes as email_blueprint
    app.register_blueprint(email_blueprint)

    from app.public_routes import public_routes as public_blueprint
    app.register_blueprint(public_blueprint)

    from app.logger import get_handlers
    file_handler, mail_handler = get_handlers(app)
    app.logger.addHandler(file_handler)
    app.logger.addHandler(mail_handler)


    if app.config["ENV"] == "production" or True:
        from app.background_tasks import delete_unconfirmed_users

        scheduler.add_job(func=delete_unconfirmed_users, id="delete_unconfirmed", trigger="interval", seconds=5, kwargs={"app":app})
        scheduler.start()

    db.create_all(app=app)

    ###-this-is-a-quickfix-to-work-with-groups-####
    from app.models import Group, User
    with app.app_context():
        groups = [
            "5a",
            "5b",
            "5c",
            "5d",
            "6a",
            "6b",
            "6c",
            "6d",
            "7a",
            "7b",
            "7c",
            "7d",
            "8a",
            "8b",
            "8c",
            "8d",
            "9a",
            "9b",
            "9c",
            "9d",
            "10a",
            "10b",
            "10c",
            "10d",
            "J1",
            "J2",
            "5_ETH_SIG",
            "10_NwT_LA",
            "J1_M1_JE",
            "J1_M2_PE",
        ]
        for group in groups:
            if not db.session.query(Group).filter(Group.name == group).all():
                db.session.add(Group(group))
        # DEBUG adding test users
        for i in range(1000):
            if not db.session.query(User).filter(User.email == f"email{i}").all():
                db.session.add(User(email=f"email{i}", first_name="Mr.", last_name=str(i), role="testuser"))
        db.session.commit()

    return app