from flask import escape
from flask_login import UserMixin
from werkzeug.security import generate_password_hash
import secrets
import datetime

from app import db, login_manager


class User(UserMixin, db.Model):
    __tablename__ = "users"

    email = db.Column(db.String, primary_key=True)
    confirmation_token = db.Column(db.String)
    unsubscribe_token = db.Column(db.String)
    registration_time = db.Column(db.DateTime)
    is_confirmed = db.Column(db.Boolean, default=False)
    password = db.Column(db.String)
    password_reset_token = db.Column(db.String)
    password_reset_time = db.Column(db.DateTime)

    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    role = db.Column(db.String)

    def __init__(self, email, first_name, last_name, role, password=None):
        self.email = email
        self.is_confirmed = False
        self.first_name = escape(first_name)
        self.last_name = escape(last_name)
        self.role = role
        if password:
            self.password = generate_password_hash(password)
        self.registration_time = datetime.datetime.now()

    def set_tokens(self, confirmation_token, unsubscribe_token):
        self.confirmation_token = confirmation_token
        self.unsubscribe_token = unsubscribe_token

    def update(self):
        self.unsubscribe_token = secrets.token_hex(16)

    def confirm(self):
        self.is_confirmed = True
        db.session.add(self)
        db.session.commit()

    def set_password_reset_token(self, token):
        self.password_reset_token = generate_password_hash(token)
        self.password_reset_time = datetime.datetime.now()
        db.session.add(self)
        db.session.commit()

    def set_new_password(self, password):
        self.password_reset_token = ""
        self.password_reset_time = None
        self.password = generate_password_hash(password)
        db.session.add(self)
        db.session.commit()

    @property
    def id(self):
        return self.email


@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)


class InGroup(db.Model):
    __tablename__ = "in_group"

    email = db.Column(db.String, primary_key=True)
    group = db.Column(db.Integer, primary_key=True)

    def __init__(self, email, group):
        self.email = email
        self.group = group

class LogEntry(db.Model):
    __tablename__ = "logentries"

    id = db.Column(db.Integer, primary_key=True)

    platform = db.Column(db.String)
    browser = db.Column(db.String)
    version = db.Column(db.String)
    user_agent = db.Column(db.String)
    page = db.Column(db.String)
    time = db.Column(db.DateTime)

    def __init__(self, page, platform, browser, version, user_agent):
        self.time = datetime.datetime.now()
        self.page = page
        self.platform = platform
        self.browser = browser
        self.version = version
        self.user_agent = user_agent


class Group(db.Model):
    __tablename__ = "groups"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    def __init__(self, name):
        self.name = name


# db.create_all()


def get_all_group_names():
    """Return the name of every group that has a particular type in a list, ordered by their type"""
    return [
        group.name
        for group in db.session.query(Group.name).all()
    ]


def get_groups_by_user(user):
    """Return the names of all groups as keys of a dict. The value is True if the user is in that group."""
    groups = get_all_group_names()
    user_groups = [group.name for group in db.session.query(Group.name).filter(InGroup.email == user.email, InGroup.group == Group.id).all()]
    user_groups = dict(
        zip(groups, [group in user_groups for group in groups])
    )
    return user_groups


def get_users_from_groups(groups, students=False, parents=False):
    """
    Return all the teachers that are in at least one of the provided Groups. If students=true, return the students
    as well. If parents=true, return the parents as well.

    Parameters:
        -groups(list): List of Strings containing the Group Names
        -students(bool): whether to return users of type student as well
        -parents(bool): whether to return users of type parent as well
    """
    if students and parents:
        # Teachers, Students and Parents
        return (
            db.session.query(User)
            .filter(
                User.email == InGroup.email,
                Group.name.in_(groups),
                Group.id == InGroup.group,
                User.is_confirmed == True,
            )
            .distinct()
            .all()
        )

    elif students:
        # Teachers and Students
        return (
            db.session.query(User)
            .filter(
                User.email == InGroup.email,
                Group.name.in_(groups),
                Group.id == InGroup.group,
                User.role != "parents",
                User.is_confirmed == True,
            )
            .distinct()
            .all()
        )

    elif parents:
        # Teachers and Parents
        return (
            db.session.query(User)
            .filter(
                User.email == InGroup.email,
                Group.name.in_(groups),
                Group.id == InGroup.group,
                User.role != "students",
                User.is_confirmed == True,
            )
            .distinct()
            .all()
        )

    # Teachers
    return (
        db.session.query(User)
        .filter(
            User.email == InGroup.email,
            Group.name.in_(groups),
            Group.id == InGroup.group,
            User.role == "teacher",
        )
        .distinct()
        .all()
    )

# db.create_all()