from flask_login import current_user
from flask_login.config import EXEMPT_METHODS
from flask import request, current_app, flash
from functools import wraps
from app import db
from app.models import LogEntry


def teacher_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if request.method in EXEMPT_METHODS:
            return func(*args, **kwargs)
        elif current_app.config.get("LOGIN_DISABLED"):
            return func(*args, **kwargs)
        elif not current_user.is_authenticated:
            return current_app.login_manager.unauthorized()
        elif not current_user.role == "teacher":
            return current_app.login_manager.unauthorized()
        elif not current_user.is_confirmed:
            flash("Bitte bestätigen Sie Ihre E-Mailaddresse")
            return current_app.login_manager.unauthorized()
        return func(*args, **kwargs)

    return decorated_view


def log_activity(func):
    @wraps(func)
    def decorated(*args, **kwargs):
        db.session.add(
            LogEntry(
                page=request.path,
                platform=request.user_agent.platform,
                browser=request.user_agent.browser,
                version=request.user_agent.version,
                user_agent=request.user_agent.string
            )
        )
        db.session.commit()

        return func(*args, **kwargs)
    return decorated