import logging
from logging import Formatter
from logging.handlers import SMTPHandler, TimedRotatingFileHandler

def get_handlers(app):
    # Set up a System that sends a mail to all Developers when something goes wrong
    mail_handler = SMTPHandler(
        mailhost=(app.config["MAIL_SERVER"], 587),
        fromaddr=app.config["MAIL_USERNAME"],
        toaddrs=app.config["ADMIN_MAILS"],
        credentials=(app.config["MAIL_USERNAME"], app.config["MAIL_PASSWORD"]),
        subject="Newsletter Error"
        )

    # Also log all warnings from the past 7 days
    file_handler = TimedRotatingFileHandler(
        filename="app/logs/warnings.log",
        when="midnight",
        backupCount=7)

    mail_handler.setLevel(logging.ERROR)
    file_handler.setLevel(logging.WARNING)

    # Format Error message for the handlers
    mail_handler.setFormatter(Formatter("""
    Message type:       %(levelname)s
    Location:           %(pathname)s:%(lineno)d
    Module:             %(module)s
    Function:           %(funcName)s
    Time:               %(asctime)s

    Message:

    %(message)s

    """))
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s '
        '[in %(pathname)s:%(lineno)d]'
    ))

    return file_handler, mail_handler