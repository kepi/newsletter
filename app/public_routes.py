from flask import Blueprint, render_template, request
from app.decorators import log_activity

public_routes = Blueprint('public_routes', __name__)

@public_routes.route("/") # Make the instructions the default landing page
@log_activity
def home():
    return render_template("instructions.html")

@public_routes.route("/privacy_policy")
@log_activity
def privacy_policy():
    return render_template("privacy_policy.html")
