# Newsletter

## Running the Newsletter-Server
### Development
Command line:
```
export FLASK_ENV=development
flask run --no-reload --no-debugger
```
VS-Code launch.json:
```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python: Flask",
            "type": "python",
            "request": "launch",
            "module": "flask",
            "env": {
                "FLASK_APP": "app",
                "FLASK_ENV": "development",
                "FLASK_DEBUG": "0"
            },
            "args": [
                "run",
                "--port", "5000",
                "--host", "127.0.0.1",
                "--no-debugger",
                "--no-reload"
            ],
            "jinja": true
        }
    ]
}
```
Now your server should run on 127.0.0.1:5000


### Production (only on Linux)
```
sudo docker-compose build
sudo docker-compose up
```
Now your server should run on 127.0.0.1:80

If you need to change the port, do so in `docker-compose.yml`

If you get
```
ERROR: for flask  Cannot start service flask: OCI runtime create failed: container_linux.go:345: starting container process caused "exec: \"./gunicorn_starter.sh\": permission denied": unknown
```
you need to run
```
sudo chmod +x gunicorn_starter.sh
```



SQLite Viewer:
http://inloop.github.io/sqlite-viewer/#


### Was wir schon haben:
- Login / Registrierung für Lehrer*innen, Schüler*innen und Eltern
- Unsubscribe und Gruppenwechsel
- Passwort für Lehrer*innen
- write ist nur für Lehrer*innen verfügbar
- Emails können mit Anhang geschrieben und versendet werden
- Keine emailadressen mehr anzeigen
- Gruppenauswahl zum Emailsenden
- Style / Darkmode (kuthy)
- Möglichkeit für Lehrer*innen ihre eigene kepimail als ReplyTo zu verwenden

### Was wir noch brauchen
- ## drop IE
- Eventuell muss noch mehr in die Datenbank (Theo)
- testen, testen, testen... (alle)
- Wenn ich als eingeloggter Lehrer auf Settings gehe dann will ich nicht meine Emails öffnen müssen
- Flashed messages zentrieren

### Seiten mit Darkmode Option
- [x] /privacy_policy @all
- [x] /confirm @all
- [x] /request_new_password @all
- [x] /set_new_password @all
- [x] /logout @all
- [x] /write @Wuelle
- [x] /login @all

### Lightmode Fehler
- group_selector hat unpassende Farben