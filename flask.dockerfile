FROM python:3.8

EXPOSE 5000

# Overwrite DEFAULT:@SECLEVEL to 0 as our mailserver does not work otherwise
COPY openssl.cnf /
COPY ["openssl.cnf","/etc/ssl/"]

COPY requirements.txt /
RUN pip3 install -r /requirements.txt

COPY . /newsletter
WORKDIR /newsletter

# Build and digest static files
ENV FLASK_ENV=development
ENV FLASK_APP=app
RUN timeout -s SIGKILL 3 flask run; exit 0
RUN flask digest compile
ENV FLASK_ENV=production

ENTRYPOINT ["sh", "./gunicorn_starter.sh"]